import asyncio
import time
from threading import Thread
from src.robots.factory import robot_factory
from src.robots.clan import robot_clan
from src.reports.event_listener import subscribe_reporting
from src.storage.event_listener import subscribe_warehouse
from src.robots.event_listener import subscribe_robots


def subscribe_events():
    subscribe_reporting()
    subscribe_warehouse()
    subscribe_robots()


class AsyncLoopThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.loop = asyncio.new_event_loop()

    def run(self):
        asyncio.set_event_loop(self.loop)
        self.loop.run_forever()


def add_start_robots_to_clan():
    robot_clan.add_robot(robot_factory())
    robot_clan.add_robot(robot_factory())


amount_of_robots_requested = 10


def main():
    start_time = time.time()
    print("start waiting")
    # init a loop in another Thread
    loop_handler = AsyncLoopThread()
    loop_handler.start()

    while len(robot_clan) < amount_of_robots_requested:
        for robot in robot_clan:
            if robot.is_ready_for_work():
                asyncio.run_coroutine_threadsafe(
                    robot.new_job(), loop=loop_handler.loop
                )

    print("your wait has ended")
    end_time = time.time()
    print(f"you have waited: {end_time -start_time} seconds")


if __name__ == "__main__":
    add_start_robots_to_clan()
    subscribe_events()
    main()
