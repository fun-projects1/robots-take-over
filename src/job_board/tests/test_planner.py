from src.job_board.planner import AbstractJobPlanner, JobPlanner
from src.jobs.job import BaseJob
from src.storage.store import Warehouse


class TestJobPlanner:
    def test_init(self):
        planner = JobPlanner()
        assert isinstance(planner, AbstractJobPlanner)

    def test_plan_next_job(self):
        planner = JobPlanner()
        job = planner.plan_next_job()
        assert isinstance(job, BaseJob)

    def test_next_job_buy_robot(self):
        warehouse = Warehouse()
        warehouse._resource_foo = 6
        warehouse._resource_cash = 3
        planner = JobPlanner()
        planner.warehouse = warehouse
        work = planner.find_required_work()
        assert work == "buy_robot"

    def test_next_job_foo_if_cash(self):
        warehouse = Warehouse()
        warehouse._resource_foo = 1
        warehouse._resource_cash = 3
        planner = JobPlanner()
        planner.warehouse = warehouse
        work = planner.find_required_work()
        assert work == "foo"

    def test_next_job_sell_foobar(self):
        warehouse = Warehouse()
        warehouse._resource_foobar = 5
        planner = JobPlanner()
        planner.warehouse = warehouse
        work = planner.find_required_work()
        assert work == "sell_foobar"

    def test_next_job_foobar(self):
        warehouse = Warehouse()
        warehouse._resource_bar = 2
        warehouse._resource_foo = 1
        planner = JobPlanner()
        planner.warehouse = warehouse
        work = planner.find_required_work()
        assert work == "foobar"

    def test_next_job_bar(self):
        warehouse = Warehouse()
        warehouse._resource_bar = 0
        warehouse._resource_foo = 2
        planner = JobPlanner()
        planner.warehouse = warehouse
        work = planner.find_required_work()
        assert work == "bar"

    def test_next_job_foo(self):
        warehouse = Warehouse()
        warehouse._resource_bar = 10
        warehouse._resource_foo = 0
        planner = JobPlanner()
        planner.warehouse = warehouse
        work = planner.find_required_work()
        assert work == "foo"
