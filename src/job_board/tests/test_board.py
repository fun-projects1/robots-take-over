from src.job_board.board import JobBoard
from src.jobs.job import BaseJob


class TestJobBoard:
    def test_init(self):
        job_board = JobBoard()
        assert isinstance(job_board, JobBoard)

    def test_request_job(self):
        job_board = JobBoard()
        request_job = job_board.request_job()
        assert isinstance(request_job, BaseJob)

    def test_generate_job(self):
        job_board = JobBoard()
        job = job_board.generate_new_job()
        assert isinstance(job, BaseJob)
