from src.job_board.planner import (
    AbstractJobPlanner,
    JobPlanner,
    FakeJobPlanner,
)
from src.jobs.job import BaseJob


class JobBoard:

    job_planner: AbstractJobPlanner = JobPlanner()

    def request_job(self) -> BaseJob:
        return self.generate_new_job()

    def generate_new_job(self) -> BaseJob:
        return self.job_planner.plan_next_job()


class FakeJobBoard(JobBoard):
    job_planner = FakeJobPlanner()


general_job_board = JobBoard()
fake_job_board = FakeJobBoard()
