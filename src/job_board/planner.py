from abc import ABC, abstractmethod
from src.jobs.factory import (
    AbstractJobFactory,
    GeneralJobFactory,
    FakeJobFactory,
)
from src.jobs.job import BaseJob
from src.storage.store import warehouse as ware_house, Warehouse


class AbstractJobPlanner(ABC):

    job_factory: AbstractJobFactory
    warehouse: Warehouse = ware_house

    @abstractmethod
    def plan_next_job(self):
        pass


class FakeJobPlanner(AbstractJobPlanner):
    job_factory = FakeJobFactory()

    def plan_next_job(self) -> BaseJob:
        return self.job_factory.create_job()


class JobPlanner(AbstractJobPlanner):
    job_factory = GeneralJobFactory()

    def plan_next_job(self) -> BaseJob:
        job_type = self.find_required_work()
        return self.job_factory.create_job(job_type)

    def find_required_work(self) -> str:

        if self.warehouse.has_or_more_cash_resource(
            3
        ) & self.warehouse.has_or_more_foo_resource(6):
            return "buy_robot"
        if self.warehouse.has_or_more_cash_resource(3):
            return "foo"
        if self.warehouse.has_or_more_foobar_resource(5):
            return "sell_foobar"
        if self.warehouse.has_or_more_foo_resource(
            1
        ) & self.warehouse.has_or_more_bar_resource(1):
            return "foobar"
        if self.warehouse.has_or_more_foo_resource(1):
            return "bar"
        return "foo"
