from unittest.mock import MagicMock

import pytest
from src.events.data_struct import EventJobData, JobErrorData
from src.events.event import subscribe, EventTypes
from src.storage.gather_resources import (
    gather_buy_robot_resources,
    gather_cash_resources,
    gather_foobar_resources,
    recover_foobar_resources,
)
from src.storage.store import Warehouse


class TestGatherResourceFooBar:
    def test_gather(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        listener_mock = MagicMock()
        subscribe(EventTypes.GATHER_RESOURCE_FOOBAR.name, listener_mock)
        warehouse = Warehouse()
        warehouse._resource_bar = 1
        warehouse._resource_foo = 1

        gather_foobar_resources(job_data, warehouse)

        listener_mock.assert_called_once_with(job_data)
        assert warehouse._resource_bar == 0
        assert warehouse._resource_foo == 0

    def test_gather_fail(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        job_error_data = JobErrorData(
            job_type=job_data.job_type,
            robot_name=job_data.robot_name,
            error_string="not enough resources",
        )
        listener_mock = MagicMock()
        listener_mock_error = MagicMock()
        subscribe(EventTypes.GATHER_RESOURCE_FOOBAR.name, listener_mock)
        subscribe(EventTypes.GATHER_RESOURCE_ERROR.name, listener_mock_error)
        warehouse = Warehouse()
        warehouse._resource_bar = 1
        warehouse._resource_foo = 0

        with pytest.raises(Exception) as error:
            gather_foobar_resources(job_data, warehouse)

        listener_mock.assert_not_called()
        listener_mock_error.assert_called_once_with(job_error_data)
        assert warehouse._resource_bar == 0
        assert warehouse._resource_foo == 0
        assert "resources not gathered and lost" in str(error.value)


class TestRecoverResourcesFooBar:
    def test_recover(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        listener_mock = MagicMock()
        subscribe(EventTypes.RECOVER_RESOURCE_FOOBAR.name, listener_mock)
        warehouse = Warehouse()
        warehouse._resource_bar = 1
        warehouse._resource_foo = 1

        recover_foobar_resources(job_data, warehouse)

        listener_mock.assert_called_once_with(job_data)
        assert warehouse._resource_bar == 2
        assert warehouse._resource_foo == 1


class TestGatherResourceCash:
    def test_gather(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        listener_mock = MagicMock()
        subscribe(EventTypes.GATHER_RESOURCE_CASH.name, listener_mock)
        warehouse = Warehouse()
        warehouse._resource_foobar = 5

        gather_cash_resources(job_data, warehouse)

        listener_mock.assert_called_once_with(job_data)
        assert warehouse._resource_foobar == 0

    def test_gather_fail(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        job_error_data = JobErrorData(
            job_type=job_data.job_type,
            robot_name=job_data.robot_name,
            error_string="not enough resources",
        )
        listener_mock = MagicMock()
        listener_mock_error = MagicMock()
        subscribe(EventTypes.GATHER_RESOURCE_CASH.name, listener_mock)
        subscribe(EventTypes.GATHER_RESOURCE_ERROR.name, listener_mock_error)
        warehouse = Warehouse()
        warehouse._resource_cash = 1

        with pytest.raises(Exception) as error:
            gather_cash_resources(job_data, warehouse)

        listener_mock.assert_not_called()
        listener_mock_error.assert_called_once_with(job_error_data)
        assert "resources not gathered and lost" in str(error.value)


class TestGatherResourceBuyRobot:
    def test_gather(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        listener_mock = MagicMock()
        subscribe(EventTypes.GATHER_RESOURCE_BUY_ROBOT.name, listener_mock)
        warehouse = Warehouse()
        warehouse._resource_cash = 5
        warehouse._resource_foo = 6

        gather_buy_robot_resources(job_data, warehouse)

        listener_mock.assert_called_once_with(job_data)
        assert warehouse._resource_cash == 2
        assert warehouse._resource_foo == 0

    def test_gather_fail(self):
        job_data = EventJobData(job_type="test", robot_name="Ernie")
        job_error_data = JobErrorData(
            job_type=job_data.job_type,
            robot_name=job_data.robot_name,
            error_string="not enough resources",
        )
        listener_mock = MagicMock()
        listener_mock_error = MagicMock()
        subscribe(EventTypes.GATHER_RESOURCE_BUY_ROBOT.name, listener_mock)
        subscribe(EventTypes.GATHER_RESOURCE_ERROR.name, listener_mock_error)
        warehouse = Warehouse()
        warehouse._resource_cash = 1

        with pytest.raises(Exception) as error:
            gather_buy_robot_resources(job_data, warehouse)

        listener_mock.assert_not_called()
        listener_mock_error.assert_called_once_with(job_error_data)
        assert "resources not gathered and lost" in str(error.value)
