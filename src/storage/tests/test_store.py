from unittest.mock import MagicMock

import pytest
from src.events.data_struct import ResourceChangeData
from src.events.event import EventTypes, subscribe
from src.storage.store import Warehouse


class TestWareHouse:
    def test_init(self):
        warehouse = Warehouse()
        assert warehouse.resource_foo == 0
        assert warehouse.resource_bar == 0

    def test_add_resource_bar(self):
        warehouse = Warehouse()
        assert warehouse.resource_bar == 0
        warehouse.add_resource_bar(2)
        assert warehouse.resource_bar == 2
        warehouse.add_resource_bar(3)
        assert warehouse.resource_bar == 5

    def test_add_resource_foo(self):
        warehouse = Warehouse()
        assert warehouse.resource_foo == 0
        warehouse.add_resource_foo(2)
        assert warehouse.resource_foo == 2
        warehouse.add_resource_foo(3)
        assert warehouse.resource_foo == 5

    def test_remove_resource_foo(self):
        warehouse = Warehouse()
        warehouse._resource_foo = 10
        warehouse.remove_resource_foo(2)
        assert warehouse.resource_foo == 8
        warehouse.remove_resource_foo(5)
        assert warehouse.resource_foo == 3

    def test_remove_raise_error_no_negative_foo(self):
        warehouse = Warehouse()
        warehouse._resource_foo = 10
        with pytest.raises(Exception) as error:
            warehouse.remove_resource_foo(12)
        assert "not enough resources" in str(error.value)

    def test_resource_change_foo(self):
        warehouse = Warehouse()
        mock_listeren = MagicMock()
        subscribe(EventTypes.RESOURCE_CHANGE.name, mock_listeren)

        warehouse.add_resource_foo(4)
        mock_listeren.assert_called_once_with(
            ResourceChangeData(
                resource_type="foo",
                old_value=0,
                new_value=4,
            )
        )

    def test_resource_change_bar(self):
        warehouse = Warehouse()
        mock_listeren = MagicMock()
        subscribe(EventTypes.RESOURCE_CHANGE.name, mock_listeren)

        warehouse.add_resource_bar(4)
        mock_listeren.assert_called_once_with(
            ResourceChangeData(
                resource_type="bar",
                old_value=0,
                new_value=4,
            )
        )

    def test_remove_resource_bar(self):
        warehouse = Warehouse()
        warehouse._resource_bar = 10
        warehouse.remove_resource_bar(2)
        assert warehouse.resource_bar == 8
        warehouse.remove_resource_bar(5)
        assert warehouse.resource_bar == 3

    def test_remove_raise_error_no_negative_bar(self):
        warehouse = Warehouse()
        warehouse._resource_bar = 10
        with pytest.raises(Exception) as error:
            warehouse.remove_resource_bar(12)
        assert "not enough resources" in str(error.value)

    def test_resource_change_foobar(self):
        warehouse = Warehouse()
        mock_listeren = MagicMock()
        subscribe(EventTypes.RESOURCE_CHANGE.name, mock_listeren)

        warehouse.add_resource_foobar(4)
        mock_listeren.assert_called_once_with(
            ResourceChangeData(
                resource_type="foobar",
                old_value=0,
                new_value=4,
            )
        )

    def test_remove_resource_foobar(self):
        warehouse = Warehouse()
        warehouse._resource_foobar = 10
        warehouse.remove_resource_foobar(2)
        assert warehouse.resource_foobar == 8
        warehouse.remove_resource_foobar(5)
        assert warehouse.resource_foobar == 3

    def test_remove_raise_error_no_negative_foobar(self):
        warehouse = Warehouse()
        warehouse._resource_foobar = 10
        with pytest.raises(Exception) as error:
            warehouse.remove_resource_foobar(12)
        assert "not enough resources" in str(error.value)

    def test_resource_change_cash(self):
        warehouse = Warehouse()
        mock_listeren = MagicMock()
        subscribe(EventTypes.RESOURCE_CHANGE.name, mock_listeren)

        warehouse.add_resource_cash(4)
        mock_listeren.assert_called_once_with(
            ResourceChangeData(
                resource_type="cash",
                old_value=0,
                new_value=4,
            )
        )

    def test_remove_resource_cash(self):
        warehouse = Warehouse()
        warehouse._resource_cash = 10
        warehouse.remove_resource_cash(2)
        assert warehouse.resource_cash == 8
        warehouse.remove_resource_cash(5)
        assert warehouse.resource_cash == 3

    def test_remove_raise_error_no_negative_cash(self):
        warehouse = Warehouse()
        warehouse._resource_cash = 10
        with pytest.raises(Exception) as error:
            warehouse.remove_resource_cash(12)
        assert "not enough resources" in str(error.value)
