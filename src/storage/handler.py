from src.events.data_struct import EventJobData
from src.storage.store import warehouse


def event_handler_generate_resource_foo(event_Job: EventJobData):
    warehouse.add_resource_foo()


def event_handler_generate_resource_bar(event_Job: EventJobData):
    warehouse.add_resource_bar()


def event_handler_generate_resource_foobar(event_Job: EventJobData):
    warehouse.add_resource_foobar()


def event_handler_generate_resource_cash(event_Job: EventJobData):
    warehouse.add_resource_cash(5)
