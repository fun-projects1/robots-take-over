from src.events.data_struct import EventJobData, JobErrorData
from src.events.event import post_events, EventTypes
from src.storage.store import Warehouse, warehouse


def gather_foobar_resources(
    jobEvent: EventJobData,
    warehouse: Warehouse = warehouse,
):

    try:
        warehouse.remove_resource_bar(1)
        warehouse.remove_resource_foo(1)
    except Exception as e:
        post_events(
            EventTypes.GATHER_RESOURCE_ERROR.name,
            JobErrorData(
                robot_name=jobEvent.robot_name,
                job_type=jobEvent.job_type,
                error_string=str(e),
            ),
        )
        raise Exception("resources not gathered and lost")
    else:
        post_events(
            EventTypes.GATHER_RESOURCE_FOOBAR.name,
            jobEvent,
        )


def gather_cash_resources(
    jobEvent: EventJobData,
    warehouse: Warehouse = warehouse,
):

    try:
        warehouse.remove_resource_foobar(5)
    except Exception as e:
        post_events(
            EventTypes.GATHER_RESOURCE_ERROR.name,
            JobErrorData(
                robot_name=jobEvent.robot_name,
                job_type=jobEvent.job_type,
                error_string=str(e),
            ),
        )
        raise Exception("resources not gathered and lost")
    else:
        post_events(
            EventTypes.GATHER_RESOURCE_CASH.name,
            jobEvent,
        )


def gather_buy_robot_resources(
    jobEvent: EventJobData,
    warehouse: Warehouse = warehouse,
):

    try:
        warehouse.remove_resource_cash(3)
        warehouse.remove_resource_foo(6)
    except Exception as e:
        post_events(
            EventTypes.GATHER_RESOURCE_ERROR.name,
            JobErrorData(
                robot_name=jobEvent.robot_name,
                job_type=jobEvent.job_type,
                error_string=str(e),
            ),
        )
        raise Exception("resources not gathered and lost")
    else:
        post_events(
            EventTypes.GATHER_RESOURCE_BUY_ROBOT.name,
            jobEvent,
        )


def recover_foobar_resources(
    jobEvent: EventJobData,
    warehouse: Warehouse = warehouse,
):
    warehouse.add_resource_bar(1)
    post_events(
        EventTypes.RECOVER_RESOURCE_FOOBAR.name,
        jobEvent,
    )
