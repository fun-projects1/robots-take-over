from src.events.event import subscribe, EventTypes
from src.storage.handler import (
    event_handler_generate_resource_foo,
    event_handler_generate_resource_bar,
    event_handler_generate_resource_foobar,
    event_handler_generate_resource_cash,
)


def subscribe_warehouse():
    subscribe(
        EventTypes.GENERATE_RESOURCE_FOO.name,
        event_handler_generate_resource_foo,
    )
    subscribe(
        EventTypes.GENERATE_RESOURCE_BAR.name,
        event_handler_generate_resource_bar,
    )
    subscribe(
        EventTypes.GENERATE_RESOURCE_FOOBAR.name,
        event_handler_generate_resource_foobar,
    )
    subscribe(
        EventTypes.GENERATE_RESOURCE_CASH.name,
        event_handler_generate_resource_cash,
    )
