from src.events.data_struct import ResourceChangeData
from src.events.event import EventTypes, post_events


class Warehouse:

    _resource_foo: int = 0
    _resource_bar: int = 0
    _resource_foobar: int = 0
    _resource_cash: int = 0

    @property
    def resource_foo(self) -> int:
        return self._resource_foo

    @resource_foo.setter
    def resource_foo(self, value: int):
        old_value = self._resource_foo
        self._resource_foo = value
        post_events(
            EventTypes.RESOURCE_CHANGE.name,
            ResourceChangeData(
                resource_type="foo",
                old_value=old_value,
                new_value=self._resource_foo,
            ),
        )

    def add_resource_foo(self, amount: int = 1):
        self.resource_foo += amount

    def remove_resource_foo(self, amount: int = 1):
        if not self.has_or_more_foo_resource(amount):
            raise Exception("not enough resources")
        self.resource_foo -= amount

    def has_or_more_foo_resource(self, amount: int) -> bool:
        return self.resource_foo >= amount

    @property
    def resource_bar(self) -> int:
        return self._resource_bar

    @resource_bar.setter
    def resource_bar(self, value: int):
        old_value = self._resource_bar
        self._resource_bar = value
        post_events(
            EventTypes.RESOURCE_CHANGE.name,
            ResourceChangeData(
                resource_type="bar",
                old_value=old_value,
                new_value=self._resource_bar,
            ),
        )

    def add_resource_bar(self, amount: int = 1):
        self.resource_bar += amount

    def remove_resource_bar(self, amount: int = 1):
        if not self.has_or_more_bar_resource(amount):
            raise Exception("not enough resources")
        self.resource_bar -= amount

    def has_or_more_bar_resource(self, amount: int) -> bool:
        return self.resource_bar >= amount

    @property
    def resource_foobar(self) -> int:
        return self._resource_foobar

    @resource_foobar.setter
    def resource_foobar(self, value: int):
        old_value = self._resource_foobar
        self._resource_foobar = value
        post_events(
            EventTypes.RESOURCE_CHANGE.name,
            ResourceChangeData(
                resource_type="foobar",
                old_value=old_value,
                new_value=self._resource_foobar,
            ),
        )

    def add_resource_foobar(self, amount: int = 1):
        self.resource_foobar += amount

    def remove_resource_foobar(self, amount: int = 1):
        if not self.has_or_more_foobar_resource(amount):
            raise Exception("not enough resources")
        self.resource_foobar -= amount

    def has_or_more_foobar_resource(self, amount: int) -> bool:
        return self.resource_foobar >= amount

    @property
    def resource_cash(self) -> int:
        return self._resource_cash

    @resource_cash.setter
    def resource_cash(self, value: int):
        old_value = self._resource_cash
        self._resource_cash = value
        post_events(
            EventTypes.RESOURCE_CHANGE.name,
            ResourceChangeData(
                resource_type="cash",
                old_value=old_value,
                new_value=self._resource_cash,
            ),
        )

    def add_resource_cash(self, amount: int = 1):
        self.resource_cash += amount

    def remove_resource_cash(self, amount: int = 1):
        if not self.has_or_more_cash_resource(amount):
            raise Exception("not enough resources")
        self.resource_cash -= amount

    def has_or_more_cash_resource(self, amount: int) -> bool:
        return self.resource_cash >= amount


warehouse = Warehouse()
