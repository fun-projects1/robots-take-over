from src.events.data_struct import (
    EventJobData,
    JobErrorData,
    ResourceChangeData,
    RobotJoinsClanData,
)


def printout_start_job(new_job: EventJobData):
    print(
        "Robot {robot_name}: started new job {job_type}".format(
            robot_name=new_job.robot_name,
            job_type=new_job.job_type,
        )
    )


def printout_finished_job(finished_job: EventJobData):
    print(
        "Robot {robot_name}: finished job {job_type}".format(
            robot_name=finished_job.robot_name,
            job_type=finished_job.job_type,
        )
    )


def printout_generate_resource_foo(job: EventJobData):
    print(f"RESOURCE GENERATED: foo by {job.robot_name}")


def printout_generate_resource_bar(job: EventJobData):
    print(f"RESOURCE GENERATED: bar by {job.robot_name}")


def printout_generate_resource_foobar(job: EventJobData):
    print(f"RESOURCE GENERATED: foobar by {job.robot_name}")


def printout_generate_resource_cash(job: EventJobData):
    print(f"RESOURCE GENERATED: cash by {job.robot_name}")


def printout_generate_resource_robot(job: EventJobData):
    print(f"RESOURCE GENERATED: robot by {job.robot_name}")


def printout_gather_resource_foo_bar(job: EventJobData):
    print(f"RESOURCE GATHERED: foo_bar by {job.robot_name}")


def printout_gather_resource_cash(job: EventJobData):
    print(f"RESOURCE GATHERED: sell foobar by {job.robot_name}")


def printout_gather_resource_buy_robot(job: EventJobData):
    print(f"RESOURCE GATHERED: buy robot by {job.robot_name}")


def printout_recover_resource_foobar(job: EventJobData):
    print(f"RESOURCE RECOVERED: bar by {job.robot_name}")


def printout_robot_joins_clan(robot: RobotJoinsClanData):
    print(f"ROBOT JOINS CLAN: {robot.robot_name}")


def printout_resouce_change(resource_change: ResourceChangeData):
    base_message = "RESOURCE CHANGE: {type} from {old_value} to {new_value}"
    message = base_message.format(
        type=resource_change.resource_type,
        old_value=resource_change.old_value,
        new_value=resource_change.new_value,
    )

    print(message)


def printout_job_error(error_message: JobErrorData):
    print(
        "Error: by{robot_name} on job {job_type} - {error_string}".format(
            robot_name=error_message.robot_name,
            job_type=error_message.job_type,
            error_string=error_message.error_string,
        )
    )


def printout_job_resource_gather_error(error_message: JobErrorData):
    message = "{type}: by{robot_name} on job {job_type} - {error_string}"
    print(
        message.format(
            type="ERROR RESOURCE GATHER",
            robot_name=error_message.robot_name,
            job_type=error_message.job_type,
            error_string=error_message.error_string,
        )
    )
