from enum import Enum
from typing import Optional
from src.job_board.board import JobBoard, general_job_board, fake_job_board
from src.jobs.job import BaseJob


class BaseRobot:
    class RobotLockStatus(Enum):
        LOCK = 1
        READY = 2

    _status = RobotLockStatus.READY.name

    _job_board: JobBoard = general_job_board

    _current_job: Optional[BaseJob] = None

    def __init__(self, name):
        self.name = name

    def new_job(self):
        self.set_to_busy()
        self.set_current_job()
        self.register_name_to_job()
        self.gather_resources()
        return self.async_job()

    async def async_job(self):
        await self.get_current_work()
        self.clean_current_job()
        self.set_to_available()

    def set_to_busy(self):
        self._status = self.RobotLockStatus.LOCK.name

    def set_to_available(self):
        self._status = self.RobotLockStatus.READY.name

    def is_ready_for_work(self):
        return self._status == self.RobotLockStatus.READY.name

    def set_current_job(self):
        if self._current_job is not None:
            raise Exception("There is already a current job set.")

        self._current_job = self.request_job()

    def register_name_to_job(self):
        job = self.get_current_job()
        job.register_robot_name(self.name)

    def get_current_job(self):
        if self._current_job is None:
            raise Exception("There is no current job set.")
        return self._current_job

    def request_job(self) -> BaseJob:
        return self._job_board.request_job()

    def clean_current_job(self):
        self._current_job = None

    def get_current_work(self):
        return self.get_current_job().work()

    def gather_resources(self):
        job = self.get_current_job()
        job.gather_resources()


class Robot(BaseRobot):
    _job_board: JobBoard = general_job_board


class FakeRobot(BaseRobot):
    _job_board: JobBoard = fake_job_board
