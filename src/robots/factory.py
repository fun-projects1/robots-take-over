from typing import Optional
from src.robots.robot import Robot
from faker import Faker

faker = Faker()


def robot_factory(robot_name: Optional[str] = None) -> Robot:
    if robot_name is None:
        robot_name = faker.first_name()
    return Robot(robot_name)
