from src.events.data_struct import EventJobData
from src.robots.clan import robot_clan
from src.robots.factory import robot_factory


def create_robot_in_clan(event: EventJobData):
    robot_clan.add_robot(robot_factory())
