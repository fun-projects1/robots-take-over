from src.events.event import subscribe, EventTypes
from src.robots.handler import create_robot_in_clan


def subscribe_robots():
    subscribe(
        EventTypes.GENERATE_RESOURCE_BUY_ROBOT.name,
        create_robot_in_clan,
    )
