from typing import List
from src.events.data_struct import RobotJoinsClanData
from src.events.event import post_events, EventTypes

from src.robots.robot import Robot


class RobotClan:
    def __init__(self):
        self._robot_list: List[Robot] = list()

    def __iter__(self):
        return iter(self._robot_list)

    def __next__(self):
        return next(self.__iter__())

    def __len__(self):
        return len(self._robot_list)

    def add_robot(self, robot: Robot):
        assert isinstance(robot, Robot)
        self.send_event(robot)
        self._robot_list.append(robot)

    def send_event(self, robot: Robot):
        post_events(
            EventTypes.ROBOT_JOINS_CLAN.name,
            RobotJoinsClanData(robot_name=robot.name),
        )


robot_clan = RobotClan()
