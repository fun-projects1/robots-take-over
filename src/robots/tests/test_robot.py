from unittest.mock import MagicMock
import pytest
from src.jobs.job import BaseJob
from src.robots.robot import FakeRobot


class TestRobot:
    def test_set_to_busy(self):
        robot = FakeRobot("faky")
        assert robot._status == FakeRobot.RobotLockStatus.READY.name
        robot.set_to_busy()
        assert robot._status != FakeRobot.RobotLockStatus.READY.name
        assert robot._status == FakeRobot.RobotLockStatus.LOCK.name

    def test_is_ready_for_work(self):
        robot = FakeRobot("faky")
        assert robot.is_ready_for_work() is True
        robot.set_to_busy()
        assert robot.is_ready_for_work() is False

    def test_set_available(self):
        robot = FakeRobot("faky")
        robot.set_to_busy()
        assert robot.is_ready_for_work() is False
        robot.set_to_available()
        assert robot.is_ready_for_work() is True

    def test_lock_on_new_job(self):
        robot = FakeRobot("faky")
        mock_async_job = MagicMock()
        robot.async_job = mock_async_job

        assert robot.is_ready_for_work() is True
        robot.new_job()
        assert robot.is_ready_for_work() is False

        mock_async_job.assert_called_once_with()

    async def test_unlock_after_complete_job(self):
        robot = FakeRobot("faky")
        robot.set_to_busy()
        robot.set_current_job()
        robot.register_name_to_job()
        assert robot.is_ready_for_work() is False
        await robot.async_job()
        assert robot.is_ready_for_work() is True

    def test_request_job(self):
        robot = FakeRobot("faky")
        job = robot.request_job()
        assert isinstance(job, BaseJob)

    def test_set_current_job(self):
        robot = FakeRobot("faky")
        assert robot._current_job is None
        robot.set_current_job()
        assert isinstance(robot._current_job, BaseJob)

    def test_set_current_job_when_job(self):
        robot = FakeRobot("faky")
        robot.set_current_job()
        with pytest.raises(Exception) as error:
            robot.set_current_job()

        assert "There is already a current job set." in str(error.value)

    def test_get_current_work_no_job(self):
        robot = FakeRobot("faky")
        with pytest.raises(Exception) as error:
            robot.get_current_work()

        assert "There is no current job set." in str(error.value)
