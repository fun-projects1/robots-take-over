import pytest
from src.robots.clan import RobotClan
from src.robots.robot import Robot


class TestRobotClan:
    def test_init(self):
        clan = RobotClan()
        assert isinstance(clan, RobotClan)
        assert clan._robot_list == []

    def test_iter_over_clan(self):

        clan = RobotClan()
        clan._robot_list = [Robot("test_iter")]
        iter = next(clan)

        assert isinstance(iter, Robot)

    def test_add_robot_too_clan(self):
        clan = RobotClan()
        robot = Robot("addToClan")
        clan.add_robot(robot)
        assert robot in clan

    def test_raise_error_not_adding_robot(self):
        clan = RobotClan()
        with pytest.raises(AssertionError):
            clan.add_robot("not a robot")

    def test_get_length_of_clan(self):
        clan = RobotClan()
        clan.add_robot(Robot("hello"))
        clan.add_robot(Robot("world"))

        assert len(clan) == 2
