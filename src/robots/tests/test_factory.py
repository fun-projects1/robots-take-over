from src.robots.robot import Robot
from src.robots.factory import robot_factory


def test_robot_factory():
    robot = robot_factory()
    assert isinstance(robot, Robot)
