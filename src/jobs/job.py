import asyncio
from typing import Optional
from random import randint
from src.events.event import post_events, EventTypes
from src.events.data_struct import EventJobData, JobErrorData
from src.storage.gather_resources import (
    gather_buy_robot_resources,
    gather_foobar_resources,
    recover_foobar_resources,
    gather_cash_resources,
)


class BaseJob:
    second_to_wait: float = 1
    job_name: str = "base"
    _robot_name: Optional[str] = None

    def pre_work(self):
        post_events(
            EventTypes.START_JOB.name,
            self.job_event_data(),
        )

    async def work(self):
        try:
            self.pre_work()
            await self.waiting_job()
            self.post_work()
        except Exception as e:
            post_events(
                EventTypes.JOB_ERROR.name,
                JobErrorData(
                    robot_name=self.robot_name,
                    job_type=self.job_name,
                    error_string=str(e),
                ),
            )

    def post_work(self):
        post_events(
            EventTypes.FINISH_JOB.name,
            self.job_event_data(),
        )

    async def waiting_job(self):
        await asyncio.sleep(self.second_to_wait)

    def register_robot_name(self, robot_name: str):
        self._robot_name = robot_name

    @property
    def robot_name(self) -> str:
        if self._robot_name is None:
            raise Exception("Robot name is not set")
        return self._robot_name

    def job_event_data(self) -> EventJobData:
        return EventJobData(
            robot_name=self.robot_name,
            job_type=self.job_name,
        )

    def gather_resources(self):
        pass


class FakeJob(BaseJob):
    second_to_wait = 0.001
    job_name = "test_job"


class MiningFooJob(BaseJob):
    second_to_wait = 1
    job_name: str = "Mining Foo"

    def post_work(self):
        post_events(
            EventTypes.GENERATE_RESOURCE_FOO.name,
            self.job_event_data(),
        )
        return super().post_work()


class MiningBarJob(BaseJob):
    job_name: str = "Mining Bar"

    def post_work(self):
        post_events(
            EventTypes.GENERATE_RESOURCE_BAR.name,
            self.job_event_data(),
        )
        return super().post_work()

    @property
    def second_to_wait(self):
        return randint(15, 20) / 10


class AssembleFooBar(BaseJob):
    second_to_wait = 2
    job_name: str = "Assemble foobar"

    def gather_resources(self):
        gather_foobar_resources(self.job_event_data())

    def post_work(self):
        self.generate_resources()
        return super().post_work()

    def generate_resources(self):

        succes_rate = randint(0, 10)
        is_successfull = succes_rate <= 6

        if is_successfull:
            post_events(
                EventTypes.GENERATE_RESOURCE_FOOBAR.name,
                self.job_event_data(),
            )
        else:
            recover_foobar_resources(self.job_event_data())


class SellFooBar(BaseJob):
    # second_to_wait = 10 # change timing to 1 second
    second_to_wait = 1
    job_name: str = "Sell foobar"

    def gather_resources(self):
        gather_cash_resources(self.job_event_data())

    def post_work(self):
        self.generate_resources()
        return super().post_work()

    def generate_resources(self):

        post_events(
            EventTypes.GENERATE_RESOURCE_CASH.name,
            self.job_event_data(),
        )


class BuyRobotJob(BaseJob):
    second_to_wait = 1
    job_name: str = "Buy Robot"

    def gather_resources(self):
        gather_buy_robot_resources(self.job_event_data())

    def post_work(self):
        self.generate_resources()
        return super().post_work()

    def generate_resources(self):
        post_events(
            EventTypes.GENERATE_RESOURCE_BUY_ROBOT.name,
            self.job_event_data(),
        )
