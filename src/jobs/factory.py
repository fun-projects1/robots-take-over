from abc import ABC
from typing import Dict, Type
from src.jobs.job import (
    BaseJob,
    FakeJob,
    MiningBarJob,
    MiningFooJob,
    AssembleFooBar,
    SellFooBar,
    BuyRobotJob,
)


class AbstractJobFactory(ABC):
    job_class_map: Dict[str, Type[BaseJob]] = {}

    def create_job(self, job_type: str) -> BaseJob:

        job_class = self.get_job_class(job_type)
        return job_class()

    def get_job_class(self, job_type) -> Type[BaseJob]:
        job_class = self.job_class_map.get(job_type)
        if job_class is None:
            raise Exception(f"job_type: {job_type} does not exists")
        return job_class


class GeneralJobFactory(AbstractJobFactory):
    job_class_map: Dict[str, Type[BaseJob]] = {
        "foo": MiningFooJob,
        "bar": MiningBarJob,
        "foobar": AssembleFooBar,
        "sell_foobar": SellFooBar,
        "buy_robot": BuyRobotJob,
    }


class FakeJobFactory(AbstractJobFactory):

    job_class_map: Dict[str, Type[BaseJob]] = {
        "fake_job": FakeJob,
    }

    def create_job(self, job_type: str = "fake_job") -> BaseJob:
        return super().create_job(job_type)
