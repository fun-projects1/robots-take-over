import pytest
from unittest.mock import MagicMock
from src.events.data_struct import EventJobData
from src.jobs.job import FakeJob, BaseJob, MiningBarJob, MiningFooJob
from src.events.event import EventTypes, subscribe


class TestJobs:
    def test_init(self):
        job = FakeJob()
        assert isinstance(job, BaseJob)

    async def test_async(self):

        job = FakeJob()
        job.register_robot_name("Ernie")

        result = await job.work()
        assert result is None

    def test_register_robot(self):
        job = FakeJob()
        job.register_robot_name("Ernie")

        assert job.robot_name == "Ernie"

    def test_register_robot_raise_error(self):
        job = FakeJob()

        with pytest.raises(Exception) as error:

            job.robot_name
        assert "Robot name is not set" in str(error.value)

    async def test_start_job_event(self):
        job = FakeJob()
        job.register_robot_name("Ernie")
        event_mock = MagicMock()
        subscribe(EventTypes.START_JOB.name, event_mock)
        job.pre_work()

        event_mock.assert_called_once_with(
            EventJobData(robot_name="Ernie", job_type=job.job_name)
        )


class TestMiningFooJob:
    def test_init(self):
        job = MiningFooJob()
        assert isinstance(job, BaseJob)

    async def test_generate_resource_foo_job_event(self):
        job = MiningFooJob()
        job.register_robot_name("Ernie")
        event_mock = MagicMock()
        subscribe(EventTypes.GENERATE_RESOURCE_FOO.name, event_mock)
        job.post_work()

        event_mock.assert_called_once_with(
            EventJobData(robot_name="Ernie", job_type=job.job_name)
        )


class TestMiningBarJob:
    def test_init(self):
        job = MiningBarJob()
        assert isinstance(job, BaseJob)

    def test_second_to_wait(self):
        job = MiningBarJob()
        second_to_wait = job.second_to_wait
        assert isinstance(second_to_wait, float)
        assert 1.5 <= second_to_wait
        assert 2 >= second_to_wait

    async def test_generate_resource_bar_job_event(self):
        job = MiningBarJob()
        job.register_robot_name("Ernie")
        event_mock = MagicMock()
        subscribe(EventTypes.GENERATE_RESOURCE_BAR.name, event_mock)
        job.post_work()

        event_mock.assert_called_once_with(
            EventJobData(robot_name="Ernie", job_type=job.job_name)
        )
