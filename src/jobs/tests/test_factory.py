import pytest
from src.jobs.factory import AbstractJobFactory, FakeJobFactory
from src.jobs.job import BaseJob


class TestJobFactory:
    def test_init(self):
        factory = FakeJobFactory()
        assert isinstance(factory, AbstractJobFactory)

    def test_create_job(self):
        factory = FakeJobFactory()
        job = factory.create_job()
        assert isinstance(job, BaseJob)

    def test_raise_error_no_job_class(self):
        factory = FakeJobFactory()
        with pytest.raises(Exception) as error:
            factory.create_job("no_job")
        assert "job_type" in str(error.value)
        assert "no_job" in str(error.value)
        assert "does not exists" in str(error.value)
