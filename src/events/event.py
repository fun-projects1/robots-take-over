from enum import Enum


subscribers = dict()


class EventTypes(Enum):
    START_JOB = 1
    FINISH_JOB = 2
    GENERATE_RESOURCE_FOO = 3
    GENERATE_RESOURCE_BAR = 4
    RESOURCE_CHANGE = 5
    JOB_ERROR = 6
    GATHER_RESOURCE_FOOBAR = 7
    GATHER_RESOURCE_ERROR = 8
    RECOVER_RESOURCE_FOOBAR = 9
    GENERATE_RESOURCE_FOOBAR = 10
    GATHER_RESOURCE_CASH = 11
    GENERATE_RESOURCE_CASH = 12
    GATHER_RESOURCE_BUY_ROBOT = 13
    GENERATE_RESOURCE_BUY_ROBOT = 14
    ROBOT_JOINS_CLAN = 15


def subscribe(event_type: str, func):
    if event_type not in subscribers:
        subscribers[event_type] = []
    subscribers[event_type].append(func)


def post_events(event_type: str, data: any):
    if event_type not in subscribers:
        return
    for func in subscribers[event_type]:
        func(data)
