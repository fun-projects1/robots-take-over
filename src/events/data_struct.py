from dataclasses import dataclass


@dataclass
class EventJobData:
    robot_name: str
    job_type: str


@dataclass
class ResourceChangeData:
    resource_type: str
    old_value: int
    new_value: int


@dataclass
class JobErrorData:
    robot_name: str
    job_type: str
    error_string: str


@dataclass
class RobotJoinsClanData:
    robot_name: str
