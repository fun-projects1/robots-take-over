from unittest.mock import MagicMock
from src.events.event import subscribe, post_events


def test_events():

    mock_func = MagicMock()
    post_data = {"hello": "world"}
    event_name = "test_event"
    subscribe(event_name, mock_func)
    mock_func.assert_not_called()
    post_events(event_name, post_data)
    mock_func.assert_called_once_with(post_data)
