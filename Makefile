RUN_LOCAL=0
CONTAINER_NAME=robot_revolution
COMPOSE_LOCAL=compose.local.yml


ifeq (${RUN_LOCAL}, 1)
RUN_COMMAND = bash -c
else
RUN_COMMAND = docker exec -it $(CONTAINER_NAME) bash -c
endif

ifeq (${COVERAGE}, 1)
COVERAGE_CONFIG = --cov --cov-config=.coveragerc
endif


## DOCKER COMPOSE
docker:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -f $(COMPOSE_LOCAL) up ${ARGS}
docker-d:
	ARGS=-d make docker
docker-build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -f $(COMPOSE_LOCAL) build ${ARGS}
docker-build-clean:
	ARGS=--no-cache make docker-build
docker-stop:
	docker compose -f $(COMPOSE_LOCAL) stop ${ARGS}
##---------------

## DOCKER
exec:
	docker exec -it $(CONTAINER_NAME) bash ${ARGS}
##---------------

## Python COMMANDS
command:
	$(RUN_COMMAND) "${COMMAND}"

## COVERAGE
coverage:
	COMMAND="coverage run -m pytest" make command
	COMMAND="coverage report" make command

coverage-html: coverage
	COMMAND="coverage html" make command
##---------------

## CLEANING
pycache-clean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
##---------------


## TESTING
test:
	COMMAND="pytest ${APP} -svv" make command

##---------------



