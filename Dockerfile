ARG PYTHON_VERSION=3.10.4-slim-bullseye

# define an alias for the specfic python version used in this file.
FROM python:${PYTHON_VERSION} as python

ARG APP_HOME=/app

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

RUN mkdir /var/log/robot

# Install apt packages
RUN apt-get update && apt-get install --no-install-recommends -y \
  # dependencies for building Python packages
  build-essential \
  && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
  && rm -rf /var/lib/apt/lists/*

WORKDIR ${APP_HOME}

RUN addgroup --system -gid 1000 robot \
    && adduser  -u 1000 --system --ingroup robot robot

ENV PATH="/home/robot/.local/bin:${PATH}"
# make robot owner of the WORKDIR directory as well.
RUN chown robot:robot ${APP_HOME} /var/log/robot

USER robot

COPY --chown=robot:robot requirements.txt requirements.txt
RUN pip install --user -r ./requirements.txt

# copy application code to WORKDIR
COPY --chown=robot:robot . ${APP_HOME}

ENTRYPOINT ["commands/entrypoint.sh"]

CMD ["commands/robot_revolution.sh"]

