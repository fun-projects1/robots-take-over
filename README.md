# Robots take over

The solution to a technical test, with some robots and stuff.

## Name

Robot Take Over

## Description

This is a technical test. you can find the full discription of the exercise in the docs folder

## Installation

### requirements

- [docker](https://docs.docker.com/engine/install/)
- [docker compose plugin](https://docs.docker.com/compose/install/compose-plugin/)
- [python](https://www.python.org/) \*recommended if using pyright/pylance for local development
- [pyenv](https://github.com/pyenv/pyenv) \*recommended if using pyright/pylance for local development

### Install docker compose development environment

```bash
make docker
```

### Install development enviroment locally

If your IDE support pylance or pyright. It's recommended to install the project locally.
To recieve all the intelegence functionality.

**create virtual env of your choice**

_example virtual env module:_

create virtual env `python -m venv .venv`  
activate enviroment: `source .venv/bin/activate`

_example pyenv:_

```bash
pyenv virtualenv 3.10.4 py-robots
pyenv local py-robots
pyenv pyright
```

**install packages**

```bash
pip install -r requirements.txt
```

## Development commands

To easy the live of the developer we use the Makefile to wrap most used commands

### Docker compose commands

```bash
make docker # start the docker applications in local
make docker-d # start the docker applications in detached mode (no stout) in local
make docker-build # build the docker images for local
make docker-build-clean # build the docker images for local with no cache
make docker-stop # stop the docker applications in local
```

### Docker exec commands

```bash
make exec # run the bash shel inside the docker container
```

### Testing

```bash
make test # run test for hole project
```

### Cleaning Commands

```bash
make pycache-clean # remove all files and folder with __pycache__
```

## Solving the problem

- I choose to solve the problem in python as I’m most familiar with the language.
- As we had multiple robots working at the same time, I wanted to solve this asynchronous.
  - 2 option asyncio python or multiple container.
  - I choose **asyncio python** as this was the perfect excuse to learn it. I already have experience working with celery and rabbitmq in multiple containers.
- To run this application I choose docker and docker-compose as this is available on most developer computers.
- I started out created 2 robots that can wait for 1 second in a while loop for 10 seconds.
  - Here I discovered I needed to create a new thread to append new waits in a while loop.
- I abstracted away work that needed to be performed into jobs. The robot needs to request the job board for a job. The robots lock itself until the job is done.
  A while loops check is the robot is available, if so it lets the robot start a new job.
- Now it was time to create proper jobs. Mining Foo and Bar. For now, these jobs are randomly chosen in the Jobboard. To store the resources created by any job. I created a Warehouse class ,that keeps track of all the resources.
  The mining job waits for the set amount of time, then it posts an event that resources have been created. There are listeners to those events that update the warehouse with the created resources.  
  Using the Observer pattern here allows to have a minimal dependencies on the Job class. All the terminal printout are events listeners.
- Next Job build foobar. This is very similar to mining jobs, but I needed to implement the gathering of resources. For this I choose to implement is simple function, that removes resources from the warehouse.  
  I choose to implement them in simple functions so the Job class does not depend on the warehouse.
  To make sure the robots got a correct job. I implemented a planner class that checks what the available resources are in the warehouse and returns the requested job type according to some simple logics.
- Next Job sell-foobar. This job was very similar to building foobar. I did not implement new logics just extended some.
- Buy Robots. This job was very similar to create foobar and sell foobar. Except for the warehouse listeners, as there is no resource robot in the warehouse.  
  There is a listener to create a new robot and add it to the clan. A RobotClan is a new abstraction around a list of robots.
  Once this was in place, I replaced the loop of 10 second for a loop of if clan smaller than 10.
  Success the program finished with 10 robots created!
- Unfortunately I experienced a bug. When planning a job, I validated if there were enough resources. Occasionally robots requested work at the same time.
  The planner planned 2 jobs but had only resources for 1. This made the job fail. It did not break the program as I catch and logged any incoming errors.  
  The best way to solve this is to reserve/lock resources on the planning face. As this solution was already way over the foreseen time estimation.  
  I solved it by simplifying the jobboard and requesting resources before starting the job asynchronous.

## Assumptions

- Change timing for selling foobar to 1 second, the program takes to long otherwise.
- Finished loop after 10 robots created, the program takes to long otherwise.
- Reduce scope: robots change of station takes 1 second is not included in this exercise. As this solution took more time than the foreseen time of this exercise.
