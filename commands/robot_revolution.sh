#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

echo "cmd: start the robot revolution"
python main.py
tail -f /dev/null

